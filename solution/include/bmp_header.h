//
// Created by Vatset Masha on 16.01.2023.
//

#ifndef IMAGE_TRANSFORMER_BMP_HEADER_H
#define IMAGE_TRANSFORMER_BMP_HEADER_H

#include "status.h"
#define HEADER_STRUCT_SIZE sizeof(struct bmp_header)
#define HEADER_FILE_TYPE 0x4D42
#define HEADER_BITS_PER_PIXEL 24
#define HEADER_SIZE 40
#define HEADER_SIZE_V2 108
#define HEADER_SIZE_V3 124
#define HEADER_PLANES_AMOUNT 1
#define HEADER_BITS_RESERVED 0
#define HEADER_COMPRESSION_TYPE 0
#define HEADER_VERTICAL_EXTENSION 0
#define HEADER_HORIZONTAL_EXTENSION 0
#define HEADER_COLORS_USED 0
#define HEADER_IMPORTANT_COLORS 0

void new_bmp_header(struct bmp_header *header, struct image const * img, unsigned long padding);
#endif //IMAGE_TRANSFORMER_BMP_HEADER_H
