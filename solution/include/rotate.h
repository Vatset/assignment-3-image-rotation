//
// Created by Vatset Masha on 06.12.2022.
//
#pragma once
#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H
#include "image.h"

struct image rotate(struct image source);
#endif //IMAGE_TRANSFORMER_ROTATE_H
