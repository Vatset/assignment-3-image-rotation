//
// Created by Vatset Masha on 06.12.2022.
//
#pragma once
#ifndef IMAGE_TRANSFORMER_STATUS_H
#define IMAGE_TRANSFORMER_STATUS_H
#include "bmpp.h"
#include "image.h"
#include <stdio.h>
enum read_status {
    READ_OK = 0,
    READ_INVALID_TYPE,
    READ_INVALID_FILE_FORMAT,
    READ_BI_SIZE_ERROR,
    READ_BI_PLANES_ERROR,

};
enum read_status bmp_read(FILE* in, struct image* img );

enum write_status{
    WRITE_OK = 0,
    WRITE_ERROR,
};
enum write_status bmp_write( FILE* out, const struct image* img );



#endif //IMAGE_TRANSFORMER_STATUS_H
