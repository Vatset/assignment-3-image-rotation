//
// Created by Vatset Masha on 16.01.2023.
//

#ifndef IMAGE_TRANSFORMER_MESSAGESTATUS_H
#define IMAGE_TRANSFORMER_MESSAGESTATUS_H

enum message_status {
    MESSAGE_ERROR_OPENING,
    MESSAGE_ERROR_CLOSING,
    MESSAGE_ERROR_READING,
    MESSAGE_ERROR_WRITING,
    MESSAGE_ERROR_NUMBER_OF_ARGS
};

const char* const message[] = {
        "Opening failed",
        "Closing failed",
        "Reading failed",
        "Writing failed",
        "Incorrect number of arguments"
};

#endif //IMAGE_TRANSFORMER_MESSAGESTATUS_H
