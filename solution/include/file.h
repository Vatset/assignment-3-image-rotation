    //
// Created by Vatset Masha on 06.12.2022.
//
#pragma once
#ifndef IMAGE_TRANSFORMER_FILE_H
#define IMAGE_TRANSFORMER_FILE_H
#include <stdio.h>

enum open_status {
    OPEN_OK ,
    OPEN_FAILED
};

enum open_status open_file(const char* filename, const char* mode, FILE** file);

enum close_status{
    CLOSE_OK,
    CLOSE_FAILED
};

enum close_status close_file(FILE* file);

#endif //IMAGE_TRANSFORMER_FILE_H
