//
// Created by Vatset Masha on 06.12.2022.
//
#pragma once
#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
#include <stdint.h>
#include <stdlib.h>
//#include <malloc.h>
struct pixel{
    uint8_t b,g,r;
};

#define PIXEL_STRUCT_SIZE sizeof(struct pixel)
struct image {
    uint64_t height, width;
    struct pixel* data;
};


struct image create_image(size_t width, size_t height);

void image_free( struct image* img);


#endif //IMAGE_TRANSFORMER_IMAGE_H
