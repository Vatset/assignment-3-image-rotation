//
// Created by Vatset Masha on 06.12.2022.
//
#include "image.h"


void image_free(struct image *img){
    free(img->data);
}


struct image create_image(size_t width, size_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(height * width * PIXEL_STRUCT_SIZE)
    };
}

