//
// Created by Vatset Masha on 06.12.2022.
//

#include "file.h"

enum open_status open_file(const char* filename, const char* mode, FILE** file){
    *file = fopen(filename, mode);
    return *file ? OPEN_OK : OPEN_FAILED;
}


enum close_status close_file(FILE* file){
    return fclose(file) == EOF ? CLOSE_FAILED : CLOSE_OK;
}
