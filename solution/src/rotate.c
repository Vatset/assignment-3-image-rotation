//
// Created by Vatset Masha on 06.12.2022.
//
#include "rotate.h"
#include "image.h"

struct image rotate(struct image const source) {
    struct image img_rotated = create_image(source.height, source.width);
    for (uint64_t row = 0; row < img_rotated.height; row++) {
        for (uint64_t column = 0; column < img_rotated.width; column++) {
            uint64_t target = row * img_rotated.width + column;
            uint64_t source_pixel = (source.height - 1 - column) * source.width + row;
            *(img_rotated.data + target) = *(source.data + source_pixel);
        }
    }
    return img_rotated;
}
