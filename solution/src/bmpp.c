//
// Created by Vatset Masha on 06.12.2022.
//
#include "bmpp.h"
#include "bmp_header.h"
#include "status.h"
#include <stdint.h>
#include <stdio.h>

static enum read_status check_file_header(struct bmp_header header){
    if (header.bfType != HEADER_FILE_TYPE || header.biBitCount != HEADER_BITS_PER_PIXEL ||header.biCompression != HEADER_COMPRESSION_TYPE) {
        return READ_INVALID_FILE_FORMAT;}
    else if (header.biSize != HEADER_SIZE && header.biSize != HEADER_SIZE_V2 && header.biSize != HEADER_SIZE_V3) {
        return READ_BI_SIZE_ERROR;
    } else if (header.biPlanes != HEADER_PLANES_AMOUNT) {
        return READ_BI_PLANES_ERROR;
    }
    return READ_OK;
}


enum read_status bmp_read( FILE * in, struct image * img){
    if (in == NULL)  return READ_INVALID_TYPE;
    struct bmp_header header;
    fread(&header, HEADER_STRUCT_SIZE, 1, in);

    const enum read_status header_status = check_file_header(header);
    if (header_status) return header_status;

    *img = create_image(header.biWidth, header.biHeight);

    unsigned long padding = ((3 * img->width + 3) & (-4)) - 3 * img->width;


    for (size_t i = 0; i < img->height; i ++) {
        fread(img->data + img->width * i, PIXEL_STRUCT_SIZE, img->width, in);
        fseek(in, (long)padding, 1);
    }

    return READ_OK;
}

enum write_status bmp_write( FILE * out, struct image const * img ){
    unsigned long padding = ((3 * img->width + 3) & (-4)) - 3 * img->width;
    struct bmp_header header;
    new_bmp_header(&header, img, padding);
    if(!fwrite(&header, HEADER_STRUCT_SIZE, 1, out)){
        return WRITE_ERROR;
    }

    for (size_t i = 0; i < img->height; i ++) {
        uint64_t write_status = fwrite(img->data + img->width * i, PIXEL_STRUCT_SIZE, img->width,out);
        if (!write_status) {
            return WRITE_ERROR;
        }
        fseek(out, (long) padding, 1);
    }
    return WRITE_OK;
}
