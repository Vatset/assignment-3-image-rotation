//
// Created by Vatset Masha on 06.12.2022.
//
#include "file.h"
#include "messageStatus.h"
#include "rotate.h"
#include "status.h"
#include <stdbool.h>


struct image read_image_from_file(const char* filename) {
    struct image image = {0};
    FILE* file;
    enum open_status open_stat = open_file(filename, "rb", &file);

    if (open_stat != OPEN_OK) {
        fprintf(stdout, "%s", message[MESSAGE_ERROR_OPENING]);
        exit(EXIT_FAILURE);
    }

    enum read_status read_stat = bmp_read(file, &image);

    if (read_stat != READ_OK) {
        fprintf(stdout, "%s", message[MESSAGE_ERROR_READING]);
        image_free(&image);
        exit(EXIT_FAILURE);
    }

    enum close_status close_stat = close_file(file);

    if (close_stat != CLOSE_OK) {
        fprintf(stdout, "%s", message[MESSAGE_ERROR_CLOSING]);
        image_free(&image);
        exit(EXIT_FAILURE);
    }

    return image;
}

bool write_image_to_file(const struct image image, const char* filename) {
    FILE* file;

    enum open_status open_stat = open_file(filename, "wb", &file);

    if (open_stat != OPEN_OK) {
        fprintf(stdout, "%s", message[MESSAGE_ERROR_OPENING]);
        return 0;
    }

    enum write_status write_stat = bmp_write(file, &image);

    if (write_stat != WRITE_OK) {
        fprintf(stdout, "%s", message[MESSAGE_ERROR_WRITING]);
        return 0;
    }

    enum close_status close_stat = close_file(file);

    if (close_stat != CLOSE_OK) {
        fprintf(stdout, "%s", message[MESSAGE_ERROR_CLOSING]);
        return 0;
    }

    return 1;
}
int main (int args, char **argv) {
    if (args != 3) {
        fprintf(stdout, "%s", message[MESSAGE_ERROR_NUMBER_OF_ARGS]);
        return EXIT_FAILURE;
    }

    char* input_file = argv[1];
    char* output_file = argv[2];

    struct image image = read_image_from_file(input_file);
    struct image rotate_img = rotate(image);

    if (!write_image_to_file(rotate_img, output_file)) {
        image_free(&image);
        image_free(&rotate_img);
        fprintf(stdout, "%s", message[MESSAGE_ERROR_WRITING]);
        return EXIT_FAILURE;
    }
    image_free(&rotate_img);
    image_free(&image);
    return EXIT_SUCCESS;
}
