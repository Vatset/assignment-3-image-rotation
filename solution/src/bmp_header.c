//
// Created by Vatset Masha on 16.01.2023.
//
#include "bmp_header.h"

void new_bmp_header(struct bmp_header *header, struct image const * img, unsigned long padding) {
    header->bfType = HEADER_FILE_TYPE;
    header->bfileSize = img->width * img->height * PIXEL_STRUCT_SIZE + img->height * padding + HEADER_STRUCT_SIZE;
    header->bfReserved = HEADER_BITS_RESERVED;
    header->bOffBits = HEADER_STRUCT_SIZE;
    header->biSize = HEADER_SIZE;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biPlanes = HEADER_PLANES_AMOUNT;
    header->biBitCount = HEADER_BITS_PER_PIXEL;
    header->biCompression = HEADER_COMPRESSION_TYPE;
    header->biXPelsPerMeter = HEADER_VERTICAL_EXTENSION;
    header->biYPelsPerMeter = HEADER_HORIZONTAL_EXTENSION;
    header->biClrUsed = HEADER_COLORS_USED;
    header->biClrImportant = HEADER_IMPORTANT_COLORS;
    header->biSizeImage = img->height * img->width;
}
